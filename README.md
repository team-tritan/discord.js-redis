# discord.js-redis

Integrates Discord.js caching with Redis. Stores users, guilds, channels, messages, and emojis in a hash set of IDs (keys are the plural of the type: e.g. `users`, `messages`, etc.). You can subscribe to channels named `[type]Set` and `[type]Delete` which will contain a payload of the resource ID.

## Example

```js
const discord = require("discord.js");
const redis = require("discordjs-redis")({
  host: "1.3.3.7", // these options can be found on redis.js.org
});

const client = new discord.Client();
redis(client);

client.login("token");
```
